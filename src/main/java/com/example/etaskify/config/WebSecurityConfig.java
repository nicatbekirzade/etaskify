package com.example.etaskify.config;

import com.example.etaskify.services.impl.UserDetailsServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String USER = "USER";
    private static final String ADMIN = "ADMIN";
    private static final String TASK_PATH = "/task/**";

    @Bean
    @Override
    public UserDetailsService userDetailsService() {
        return new UserDetailsServiceImpl();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService())
                .passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers("/organization/**").permitAll()
                .antMatchers(HttpMethod.GET, TASK_PATH).hasAnyAuthority(USER, ADMIN)
                .antMatchers(HttpMethod.POST, TASK_PATH).hasAuthority(ADMIN)
                .antMatchers(HttpMethod.DELETE, TASK_PATH).hasAuthority(ADMIN)
                .antMatchers(HttpMethod.PUT, TASK_PATH).hasAnyAuthority(ADMIN, USER)
                .antMatchers("/admin/**").hasAuthority(ADMIN)
                .anyRequest().authenticated()
                .and().csrf().disable()
                .formLogin().disable();
    }
}
