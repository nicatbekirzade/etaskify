package com.example.etaskify.domain.enums;

public enum TaskStatus {
    COMPLETED, IN_PROGRESS
}
