package com.example.etaskify.repository;

import com.example.etaskify.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Long> {

    Optional<Task> findByIdAndUsersId(Long taskId, Long userId);
}
