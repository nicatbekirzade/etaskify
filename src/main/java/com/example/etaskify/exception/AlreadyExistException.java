package com.example.etaskify.exception;

public class AlreadyExistException extends RuntimeException {

    private static final long serialVersionUID = 8082L;

    public AlreadyExistException(String message) {
        super(message);
    }
}
