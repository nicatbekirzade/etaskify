package com.example.etaskify.exception;

public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = 8081L;

    public NotFoundException(String message) {
        super(message);
    }
}
