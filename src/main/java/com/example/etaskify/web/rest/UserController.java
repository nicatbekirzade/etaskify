package com.example.etaskify.web.rest;

import com.example.etaskify.services.UserService;
import com.example.etaskify.services.dto.UserDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Positive;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/admin")
public class UserController {

    private final UserService userService;

    @PostMapping("/organization/{id}/user")
    @ResponseStatus(HttpStatus.CREATED)
    public void createNewUser(
            @PathVariable("id") @Positive(message = "needed positive value") Long ownerId,
            @RequestBody @Validated UserDto userDto) {
        userService.createUser(ownerId, userDto);
    }

}
