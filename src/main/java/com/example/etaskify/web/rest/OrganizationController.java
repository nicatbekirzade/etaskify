package com.example.etaskify.web.rest;

import com.example.etaskify.services.OrganizationService;
import com.example.etaskify.services.UserService;
import com.example.etaskify.services.dto.AdminDto;
import com.example.etaskify.services.dto.OrganizationDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Positive;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/organization")
public class OrganizationController {

    private final OrganizationService organizationService;
    private final UserService userService;

    @PostMapping
    public void createOrganization(@RequestBody @Validated OrganizationDto organizationDto) {
        organizationService.createOrganization(organizationDto);
    }

    @PostMapping("/{organizationId}/admin")
    @ResponseStatus(HttpStatus.CREATED)
    public void createOrganizationAdmin(
            @PathVariable("organizationId") @Positive(message = "needed positive value") Long organizationId,
            @RequestBody @Validated AdminDto adminDto) {
        userService.createOwner(organizationId, adminDto);
    }
}
