package com.example.etaskify.web.rest;

import com.example.etaskify.services.TaskService;
import com.example.etaskify.services.dto.TaskDto;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@Validated
@RequiredArgsConstructor
@RequestMapping("/task")
public class TaskController {

    private final TaskService taskService;
    private static final String POSITIVE_VALUE = "needed positive value";

    //    @Secured("ROLE_ADMIN")
    @PutMapping("/assign/{userId}/{taskId}")
    public void assignTaskToUser(@PathVariable("userId") Long userId, @PathVariable("taskId") Long taskId) {
        taskService.assignTaskToUser(userId, taskId);
    }

    @GetMapping("/findAll")
    public List<TaskDto> getTasks() {
        return taskService.findAllTasks();
    }

    @PutMapping("/set-done/{id}")
    public void setTaskCompleted(@PathVariable("id") @Positive(message = POSITIVE_VALUE) Long id) {
        taskService.setTaskCompleted(id);
    }

    @PutMapping("/set-in-progress/{id}")
    public void setTaskInProgress(@PathVariable("id") @Positive(message = POSITIVE_VALUE) Long id) {
        taskService.setTaskInProgress(id);
    }

    @PostMapping("/{id}")
    public void createTask(@PathVariable("id") @Positive(message = POSITIVE_VALUE) Long ownerId,
                           @RequestBody @Validated TaskDto taskDto) {
        taskService.createTask(ownerId, taskDto);
    }

    @PutMapping("/{id}")
    public void updateTask(@RequestBody @Validated TaskDto taskDto,
                           @PathVariable("id") @Positive(message = POSITIVE_VALUE) Long id) {
        taskService.updateTask(id, taskDto);
    }

    @DeleteMapping("/{id}")
    public void deleteTask(@PathVariable("id") @Positive(message = POSITIVE_VALUE) Long id) {
        taskService.deleteTask(id);
    }
}
