package com.example.etaskify.services;

import com.example.etaskify.services.dto.AdminDto;
import com.example.etaskify.services.dto.UserDto;

public interface UserService {

    void createUser(Long ownerId, UserDto userDto);

    void createOwner(Long organizationId, AdminDto adminDto);
}
