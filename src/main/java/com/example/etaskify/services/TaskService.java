package com.example.etaskify.services;

import com.example.etaskify.services.dto.TaskDto;

import java.util.List;

public interface TaskService {


    void updateTask(Long taskId, TaskDto taskDto);

    void createTask(Long ownerId, TaskDto task);

    void deleteTask(Long taskId);

    List<TaskDto> findAllTasks();

    void setTaskCompleted(Long taskId);

    void setTaskInProgress(Long taskId);

    void assignTaskToUser(Long userId, Long taskId);

}
