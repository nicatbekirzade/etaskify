package com.example.etaskify.services.impl;

import com.example.etaskify.domain.Organization;
import com.example.etaskify.domain.Role;
import com.example.etaskify.domain.User;
import com.example.etaskify.exception.AlreadyExistException;
import com.example.etaskify.exception.NotFoundException;
import com.example.etaskify.repository.OrganizationRepository;
import com.example.etaskify.repository.RoleRepository;
import com.example.etaskify.repository.UserRepository;
import com.example.etaskify.services.UserService;
import com.example.etaskify.services.dto.AdminDto;
import com.example.etaskify.services.dto.UserDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private static final String ADMIN = "ADMIN";
    private static final String USER = "USER";

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final ModelMapper modelMapper;
    private final OrganizationRepository organizationRepository;


    @Override
    @Transactional
    public void createUser(Long adminId, UserDto userDto) {
        log.info("Create new user for Organization:");
        checkUserNameAlreadyInUse(userDto.getUserName());
        checkEmailAlreadyInUse(userDto.getEmail());
        userDto.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        User admin = findUserById(adminId);
        User user = modelMapper.map(userDto, User.class);
        Role userRole = getUserRole();
        user.setActive(true);
        user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        user.setOwner(admin);
        admin.getEmployees().add(user);
    }

    @Override
    @Transactional
    public void createOwner(Long organizationId, AdminDto adminDto) {
        checkUserNameAlreadyInUse(adminDto.getUserName());
        checkEmailAlreadyInUse(adminDto.getEmail());
        Organization organization = findOrganizationById(organizationId);
        adminDto.setPassword(bCryptPasswordEncoder.encode(adminDto.getPassword()));
        User admin = modelMapper.map(adminDto, User.class);
        organization.setOwner(admin);
        admin.setActive(true);
        Role userRole = getAdminRole();
        admin.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
    }

    private Organization findOrganizationById(Long organizationId) {
        return organizationRepository.findById(organizationId)
                .orElseThrow(() -> new NotFoundException(
                        String.format("Organization with: '%d' does not exist", organizationId)));
    }

    private void checkEmailAlreadyInUse(String email) {
        userRepository.findByEmail(email)
                .ifPresent(u -> {
                    throw new AlreadyExistException(
                            String.format("User by email: '%s' already exists", email));
                });
    }

    private void checkUserNameAlreadyInUse(String userName) {
        userRepository.findByUserName(userName).ifPresent(u -> {
            throw new AlreadyExistException(String.format(
                    "User by user name : '%s' already exists", userName));
        });
    }

    private Role getUserRole() {
        Optional<Role> optionalRole = roleRepository.findByName(USER);
        if (optionalRole.isEmpty()) {
            Role role = new Role();
            role.setName(USER);
            return roleRepository.save(role);
        }
        return optionalRole.get();
    }

    private Role getAdminRole() {
        Optional<Role> optionalRole = roleRepository.findByName(ADMIN);
        if (optionalRole.isEmpty()) {
            Role role = new Role();
            role.setName(ADMIN);
            return roleRepository.save(role);
        }
        return optionalRole.get();
    }

    private User findUserById(Long ownerId) {
        return userRepository.findById(ownerId).orElseThrow(() -> new NotFoundException(
                String.format("User with id: '%d' does not exist", ownerId)
        ));
    }
}

