package com.example.etaskify.services.impl;

import com.example.etaskify.domain.Address;
import com.example.etaskify.domain.Organization;
import com.example.etaskify.exception.AlreadyExistException;
import com.example.etaskify.repository.AddressRepository;
import com.example.etaskify.repository.OrganizationRepository;
import com.example.etaskify.services.OrganizationService;
import com.example.etaskify.services.dto.OrganizationDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class OrganizationServiceImpl implements OrganizationService {

    private final OrganizationRepository organizationRepository;
    private final AddressRepository addressRepository;
    private final ModelMapper modelMapper;

    @Override
    @Transactional
    public void createOrganization(OrganizationDto organizationDto) {
        Organization org = modelMapper.map(organizationDto, Organization.class);
        checkOrganizationNameAlreadyInUse(organizationDto.getName());
        org.setAddress(modelMapper.map(organizationDto.getAddress(), Address.class));
        organizationRepository.save(org);
    }

    private void checkOrganizationNameAlreadyInUse(String organizationName) {
        organizationRepository.findByName(organizationName).ifPresent(o -> {
            throw new AlreadyExistException(
                    String.format("Organization with name: '%s' already exists", organizationName));
        });
    }
}
