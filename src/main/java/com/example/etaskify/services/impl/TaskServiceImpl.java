package com.example.etaskify.services.impl;

import com.example.etaskify.domain.Task;
import com.example.etaskify.domain.User;
import com.example.etaskify.domain.enums.TaskStatus;
import com.example.etaskify.exception.AlreadyExistException;
import com.example.etaskify.exception.NotFoundException;
import com.example.etaskify.repository.TaskRepository;
import com.example.etaskify.repository.UserRepository;
import com.example.etaskify.services.TaskService;
import com.example.etaskify.services.dto.TaskDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    @Autowired
    private EmailSenderService emailSenderService;

    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    @Override
    public void deleteTask(Long taskId) {
        Task taskById = findTaskById(taskId);
        taskRepository.delete(taskById);
    }

    @Override
    public List<TaskDto> findAllTasks() {
        return taskRepository.findAll().stream()
                .map(t -> modelMapper.map(t, TaskDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public void createTask(Long ownerId, TaskDto taskDto) {
        User user = findUserById(ownerId);
        Task task = modelMapper.map(taskDto, Task.class);
        task.setOwner(user);
        taskRepository.save(task);
    }

    @Override
    public void updateTask(Long taskId, TaskDto taskDto) {
        Task task = findTaskById(taskId);
        task.setTitle(taskDto.getTitle());
        task.setDescription(taskDto.getDescription());
        taskRepository.save(task);
    }

    @Override
    public void setTaskCompleted(Long taskId) {
        Task task = findTaskById(taskId);
        task.setStatus(TaskStatus.COMPLETED);
        taskRepository.save(task);
    }

    @Override
    public void setTaskInProgress(Long taskId) {
        Task task = findTaskById(taskId);
        task.setStatus(TaskStatus.IN_PROGRESS);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void assignTaskToUser(Long userId, Long taskId) {
        taskRepository.findByIdAndUsersId(taskId, userId).ifPresent(task -> {
            throw new AlreadyExistException(
                    String.format("Task with id: '%d' already assigned to user whits id: '%d'", taskId, userId));
        });
        Task task = findTaskById(taskId);
        User selectedUser = findUserById(userId);
        task.getUsers().add(selectedUser);
        taskRepository.save(task);
        sendEmail(selectedUser.getEmail(), task);
    }


    private Task findTaskById(Long taskId) {
        return taskRepository.findById(taskId).orElseThrow(() -> new NotFoundException(
                String.format("Task with id: '%d' does not exist", taskId)));
    }

    private User findUserById(Long ownerId) {
        return userRepository.findById(ownerId).orElseThrow(() -> new NotFoundException(
                String.format("User with id: '%d' does not exist", ownerId)
        ));
    }

    private void sendEmail(String email, Task task) {
        log.info("send email to user");
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(email);
        mailMessage.setSubject(task.getTitle() + " has assigned you!");
        mailMessage.setFrom("businesshub808@gmail.com");
        mailMessage.setText(task.getDescription());
        emailSenderService.sendSimpleMessage(mailMessage);
    }
}
