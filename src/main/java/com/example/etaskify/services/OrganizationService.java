package com.example.etaskify.services;

import com.example.etaskify.services.dto.OrganizationDto;

public interface OrganizationService {

    void createOrganization(OrganizationDto organizationDto);
}
