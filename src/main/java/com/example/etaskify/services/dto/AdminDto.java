package com.example.etaskify.services.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class AdminDto {

    @Size(min = 5, message = "Required minimum 5 characters")
    @NotBlank(message = "username is required")
    private String userName;

    @Email(message = "Valid Email address")
    @NotBlank(message = "email is required")
    private String email;

    @Size(min = 4, message = "Required minimum 4 characters for password")
    @NotBlank(message = "password is required")
    private String password;
}
