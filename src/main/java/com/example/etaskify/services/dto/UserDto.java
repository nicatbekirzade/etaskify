package com.example.etaskify.services.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Data
public class UserDto {

    @Size(min = 5, message = "Your user name must have at least 5 characters")
    @NotBlank(message = "username is required")
    private String userName;

    @Email(message = "Valid Email address")
    @NotBlank(message = "email is required")
    private String email;

    @Size(min = 4, message = "Your password must have at least 4 characters")
    @NotBlank(message = "password is required")
    private String password;

    @NotBlank(message = "name is required")
    private String name;

    @NotBlank(message = "surname is required")
    private String surname;

}
