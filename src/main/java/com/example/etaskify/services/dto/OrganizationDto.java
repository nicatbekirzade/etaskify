package com.example.etaskify.services.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class OrganizationDto {

    @NotBlank(message = "Organization name: ")
    private String name;

    @NotBlank(message = "Organization phone number: ")
    private String phone;

    private AddressDto address;

}
