package com.example.etaskify.services.dto;

import lombok.Data;

@Data
public class AddressDto {

    private String city;

    private String place;

}
