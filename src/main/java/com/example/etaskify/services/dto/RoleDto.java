package com.example.etaskify.services.dto;

import lombok.Data;

@Data
public class RoleDto {

    private String role;
}
