package com.example.etaskify.services.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Data
public class TaskDto {

    @NotBlank(message = "task title is required")
    private String title;

    @NotBlank(message = "task description is required")
    private String description;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate deadline;
}
