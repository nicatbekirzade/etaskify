package com.example.etaskify.service;

import com.example.etaskify.domain.Address;
import com.example.etaskify.domain.Organization;
import com.example.etaskify.domain.User;
import com.example.etaskify.exception.NotFoundException;
import com.example.etaskify.repository.OrganizationRepository;
import com.example.etaskify.repository.RoleRepository;
import com.example.etaskify.repository.UserRepository;
import com.example.etaskify.services.dto.AdminDto;
import com.example.etaskify.services.dto.UserDto;
import com.example.etaskify.services.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    private static final Long ID = 1L;
    private static final String EMAIL = "info@example.com";
    private static final String PASSWORD = "password";
    private static final String NAME = "firstname";
    private static final String SURNAME = "lastname";
    private static final String PHONE = "+994";
    private static final String CITY = "city";
    private static final String PLACE = "place";
    private static final String ORGANIZATION_NAME = "organization_name";
    private static final String USERNAME = "username";

    private static final String ERROR_MESSAGE =
            String.format("Organization with: '%d' does not exist", ID);

    @Mock
    private UserRepository userRepository;

    @Mock
    @SuppressWarnings({"PMD.UnusedPrivateField"})
    private RoleRepository roleRepository;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Mock
    private ModelMapper modelMapper;

    @Mock
    private OrganizationRepository organizationRepository;

    @InjectMocks
    private UserServiceImpl userService;

    private User user;
    private User admin;
    private UserDto userDto;
    private AdminDto adminDto;
    private Organization organization;

    @Test
    public void givenUserWhenCreateUserThenCreated() {
        //Arrange
        when(userRepository.findById(ID)).thenReturn(Optional.ofNullable(admin));
        when(userRepository.findByUserName(USERNAME)).thenReturn(Optional.empty());
        when(userRepository.findByEmail(EMAIL)).thenReturn(Optional.empty());
        when(modelMapper.map(userDto, User.class)).thenReturn(user);

        //Act
        userService.createUser(ID, userDto);

        //Assert
        assertThat(user.getEmail()).isEqualTo(EMAIL);
    }

    @Test
    public void givenOwnerWhenCreateOwnerExpectOwnerCreated() {
        //Arrange
        when(organizationRepository.findById(ID)).thenReturn(Optional.ofNullable(organization));
        when(bCryptPasswordEncoder.encode(PASSWORD)).thenReturn(user.getPassword());
        when(modelMapper.map(adminDto, User.class)).thenReturn(admin);

        //Act & Assert
        userService.createOwner(ID, adminDto);

        //Assert
        assertThat(organization.getOwner()).isEqualTo(admin);
    }

    @Test
    public void givenOrganizationIsNotPresentWhenCreateExpectErrorMessage() {
        //Arrange
        when(organizationRepository.findById(ID)).thenReturn(Optional.empty());

        //Act & Assert
        assertThatThrownBy(() -> userService.createOwner(ID, adminDto))
                .isInstanceOf(NotFoundException.class)
                .hasMessage(ERROR_MESSAGE);
    }

    @Test
    public void givenOwnerIsNotPresentWhenCreateUserExpectErrorMessage() {
        //Arrange
        when(userRepository.findById(ID)).thenReturn(Optional.empty());

        //Act & Assert
        assertThatThrownBy(() -> userService.createUser(ID, userDto))
                .isInstanceOf(NotFoundException.class)
                .hasMessage(String.format("User with id: '%d' does not exist", ID));
    }

    @BeforeEach
    @SuppressWarnings("checkstyle:methodlength")
    void setUp() {

        Address address = new Address();
        address.setId(ID);
        address.setCity(CITY);
        address.setPlace(PLACE);

        organization = new Organization();
        organization.setId(ID);
        organization.setName(ORGANIZATION_NAME);
        organization.setPhone(PHONE);
        organization.setAddress(address);

        admin = new User();
        admin.setUserName(USERNAME);
        admin.setId(ID);
        admin.setEmail(EMAIL);
        admin.setPassword(PASSWORD);

        user = new User();
        user.setUserName(USERNAME);
        user.setId(ID);
        user.setEmail(EMAIL);
        user.setPassword(PASSWORD);
        user.setName(NAME);
        user.setSurname(SURNAME);
        user.setOwner(admin);

        userDto = new UserDto();
        userDto.setUserName(USERNAME);
        userDto.setEmail(EMAIL);
        userDto.setName(NAME);
        userDto.setSurname(SURNAME);
        userDto.setPassword(PASSWORD);

        adminDto = new AdminDto();
        adminDto.setEmail(EMAIL);
        adminDto.setPassword(PASSWORD);
        adminDto.setUserName(USERNAME);
    }
}
