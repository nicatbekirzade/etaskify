package com.example.etaskify.service;

import com.example.etaskify.domain.Address;
import com.example.etaskify.domain.Organization;
import com.example.etaskify.exception.AlreadyExistException;
import com.example.etaskify.repository.AddressRepository;
import com.example.etaskify.repository.OrganizationRepository;
import com.example.etaskify.services.dto.AddressDto;
import com.example.etaskify.services.dto.OrganizationDto;
import com.example.etaskify.services.impl.OrganizationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrganizationServiceImplTest {

    private static final Long ID = 1L;
    public static final String ORGANIZATION_NAME = "organization_name";
    public static final String PHONE = "+994";
    public static final String CITY = "city";
    public static final String PLACE = "place";
    public static final String ALREADY_EXIST_MESSAGE =
            String.format("Organization with name: '%s' already exists", ORGANIZATION_NAME);

    @Mock
    private OrganizationRepository organizationRepository;

    @Mock
    private AddressRepository addressRepository;

    @Mock
    private ModelMapper modelMapper;

    @InjectMocks
    private OrganizationServiceImpl organizationService;

    private Organization organization;
    private OrganizationDto organizationDto;
    private Address address;

    @Test
    public void givenOrganizationIsPresentWhenCreateThenThrowAlreadyExistException() {
        //Arrange
        when(organizationRepository.findByName(ORGANIZATION_NAME))
                .thenReturn(Optional.ofNullable(organization));
        when(modelMapper.map(organizationDto, Organization.class)).thenReturn(organization);

        //Act & Assert
        assertThatThrownBy(() -> organizationService.createOrganization(organizationDto))
                .isInstanceOf(AlreadyExistException.class)
                .hasMessage(ALREADY_EXIST_MESSAGE);
    }

    @Test
    public void givenOrganizationWhenCreateOrganizationExpectOrganizationCreated() {
        //Arrange
        when(organizationRepository.findByName(ORGANIZATION_NAME))
                .thenReturn(Optional.empty());
        when(modelMapper.map(organizationDto, Organization.class)).thenReturn(organization);
        when(modelMapper.map(organizationDto.getAddress(), Address.class)).thenReturn(address);

        //Act
        organizationService.createOrganization(organizationDto);

        //Assert
        assertThat(organization.getAddress()).isEqualTo(address);

        verify(organizationRepository).findByName(ORGANIZATION_NAME);
        verify(organizationRepository).save(organization);
    }

    @BeforeEach
    void setUp() {
        address = new Address();
        address.setId(ID);
        address.setCity(CITY);
        address.setPlace(PLACE);

        AddressDto addressDto = new AddressDto();
        addressDto.setCity(CITY);
        addressDto.setPlace(PLACE);

        organization = new Organization();
        organization.setId(ID);
        organization.setName(ORGANIZATION_NAME);
        organization.setPhone(PHONE);
        organization.setAddress(address);

        organizationDto = new OrganizationDto();
        organizationDto.setName(ORGANIZATION_NAME);
        organizationDto.setPhone(PHONE);
        organizationDto.setAddress(addressDto);

    }
}
