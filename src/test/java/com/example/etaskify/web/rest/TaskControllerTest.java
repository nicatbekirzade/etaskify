package com.example.etaskify.web.rest;

import com.example.etaskify.exception.NotFoundException;
import com.example.etaskify.repository.UserRepository;
import com.example.etaskify.services.TaskService;
import com.example.etaskify.services.dto.TaskDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static com.example.etaskify.config.GlobalExceptionHandler.ERROR;
import static com.example.etaskify.config.GlobalExceptionHandler.ERRORS;
import static com.example.etaskify.config.GlobalExceptionHandler.MESSAGE;
import static com.example.etaskify.config.GlobalExceptionHandler.PATH;
import static com.example.etaskify.config.GlobalExceptionHandler.STATUS;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = TaskController.class)
@WithMockUser(authorities = {"ADMIN", "USER"})
class TaskControllerTest {

    private static final Long ID = 1L;
    private static final String TITLE = "Title";
    private static final String DESCRIPTION = "Description";
    private static final LocalDate DEADLINE = LocalDate.now();

    private static final String ASSIGN_TASK_TO_USER_PATH = "/task/assign/{userId}/{taskId}";
    private static final String ERROR_ASSIGN_TASK_TO_USER_PATH = "/task/assign/1/1";
    private static final String CREATE_TASK_PATH = "/task/{ownerId}";
    private static final String GET_ALL_TASK_PATH = "/task/findAll";
    private static final String UPDATE_TASK_PATH = "/task/{id}";
    private static final String DELETE_TASK_PATH = "/task/{id}";
    private static final String ERROR_DELETE_TASK_PATH = "/task/1";
    private static final String ERROR_UPDATE_TASK_PATH = "/task/1";
    private static final String SET_TASK_COMPLETED_PATH = "/task/set-done/{id}";
    private static final String SET_TASK_IN_PROGRESS_PATH = "/task/set-in-progress/{id}";
    private static final String ERROR_TASK_IN_PROGRESS_PATH = "/task/set-in-progress/1";
    private static final String ERROR_TASK_COMPLETED_PATH = "/task/set-done/1";
    private static final String ERROR_TASK_PATH = "/task/1";
    private static final String ARGUMENT_VALIDATION_FAILED = "Argument validation failed";
    private static final String ERRORS_MESSAGE = ERRORS + "[0].message";

    private static final String TASK_NOT_FOUND_EXCEPTION_MESSAGE =
            String.format("Task with id: '%d' does not exist", ID);
    private static final String USER_NOT_FOUND_EXCEPTION_MESSAGE =
            String.format("User with id: '%d' does not exist", ID);


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TaskService taskService;

    @MockBean
    @SuppressWarnings({"PMD.UnusedPrivateField"})
    private UserRepository userRepository;

    private TaskDto taskDto;

    @BeforeEach
    void setUp() {

        taskDto = new TaskDto();
        taskDto.setTitle(TITLE);
        taskDto.setDescription(DESCRIPTION);
        taskDto.setDeadline(DEADLINE);

    }

    @Test
    public void givenTaskWhenCreateTaskExpectStatusOk() throws Exception {

        mockMvc.perform(post(CREATE_TASK_PATH, ID)
                .content(objectMapper.writeValueAsString(taskDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }


    @Test
    public void givenTaskWhenSetTaskCompletedExpectStatusOk() throws Exception {

        mockMvc.perform(put(SET_TASK_COMPLETED_PATH, ID)
                .content(objectMapper.writeValueAsString(taskDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void givenTaskNotPresentWhenSetTaskCompletedExpectErrorMessage() throws Exception {

        doThrow(new NotFoundException(TASK_NOT_FOUND_EXCEPTION_MESSAGE))
                .when(taskService).setTaskCompleted(ID);

        mockMvc.perform(put(SET_TASK_COMPLETED_PATH, ID)
                .content(objectMapper.writeValueAsString(taskDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(STATUS, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR, is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath(MESSAGE, is(TASK_NOT_FOUND_EXCEPTION_MESSAGE)))
                .andExpect(jsonPath(PATH, is(ERROR_TASK_COMPLETED_PATH)));
    }

    @Test
    public void givenTaskWhenSetTaskInProgressExpectStatusOk() throws Exception {

        mockMvc.perform(put(SET_TASK_IN_PROGRESS_PATH, ID)
                .content(objectMapper.writeValueAsString(taskDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void givenTaskNotPresentWhenSetTaskInProgressExpectStatusOk() throws Exception {

        doThrow(new NotFoundException(TASK_NOT_FOUND_EXCEPTION_MESSAGE))
                .when(taskService).setTaskInProgress(ID);

        mockMvc.perform(put(SET_TASK_IN_PROGRESS_PATH, ID)
                .content(objectMapper.writeValueAsString(taskDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(STATUS, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR, is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath(MESSAGE, is(TASK_NOT_FOUND_EXCEPTION_MESSAGE)))
                .andExpect(jsonPath(PATH, is(ERROR_TASK_IN_PROGRESS_PATH)));
    }

    @Test
    public void givenTaskTitleNotPresentWhenCreateTaskExpectErrorMessage() throws Exception {

        taskDto.setTitle(null);

        mockMvc.perform(post(CREATE_TASK_PATH, ID)
                .content(objectMapper.writeValueAsString(taskDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(STATUS, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR, is(HttpStatus.BAD_REQUEST.getReasonPhrase())))
                .andExpect(jsonPath(MESSAGE, is(ARGUMENT_VALIDATION_FAILED)))
                .andExpect(jsonPath(ERRORS_MESSAGE, is("task title is required")))
                .andExpect(jsonPath(PATH, is(ERROR_TASK_PATH)));
    }

    @Test
    public void givenTaskDescriptionNotPresentWhenCreateTaskExpectErrorMessage() throws Exception {

        taskDto.setDescription(null);

        mockMvc.perform(post(CREATE_TASK_PATH, ID)
                .content(objectMapper.writeValueAsString(taskDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(STATUS, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR, is(HttpStatus.BAD_REQUEST.getReasonPhrase())))
                .andExpect(jsonPath(MESSAGE, is(ARGUMENT_VALIDATION_FAILED)))
                .andExpect(jsonPath(ERRORS_MESSAGE, is("task description is required")))
                .andExpect(jsonPath(PATH, is(ERROR_TASK_PATH)));
    }

    @Test
    public void givenTaskWhenUpdateTaskExpectStatusOk() throws Exception {

        mockMvc.perform(put(UPDATE_TASK_PATH, ID)
                .content(objectMapper.writeValueAsString(taskDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void givenTaskNotPresentWhenUpdateTaskExpectErrorMessage() throws Exception {

        doThrow(new NotFoundException(TASK_NOT_FOUND_EXCEPTION_MESSAGE))
                .when(taskService).updateTask(ID, taskDto);

        mockMvc.perform(put(UPDATE_TASK_PATH, ID)
                .content(objectMapper.writeValueAsString(taskDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(STATUS, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR, is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath(MESSAGE, is(TASK_NOT_FOUND_EXCEPTION_MESSAGE)))
                .andExpect(jsonPath(PATH, is(ERROR_UPDATE_TASK_PATH)));
    }

    @Test
    public void givenTaskNotPresentWhenDeleteTaskExpectErrorMessage() throws Exception {

        doThrow(new NotFoundException(TASK_NOT_FOUND_EXCEPTION_MESSAGE))
                .when(taskService).deleteTask(ID);

        mockMvc.perform(delete(DELETE_TASK_PATH, ID)
                .content(objectMapper.writeValueAsString(taskDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(STATUS, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR, is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath(MESSAGE, is(TASK_NOT_FOUND_EXCEPTION_MESSAGE)))
                .andExpect(jsonPath(PATH, is(ERROR_DELETE_TASK_PATH)));
    }

    @Test
    public void givenTaskWhenDeleteTaskExpectStatusOk() throws Exception {

        mockMvc.perform(delete(DELETE_TASK_PATH, ID)
                .content(objectMapper.writeValueAsString(taskDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void givenTasksWhenGetAllTasksExpectStatusOk() throws Exception {

        mockMvc.perform(get(GET_ALL_TASK_PATH)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void givenTaskWhenAssignTaskToUserExpectStatusOk() throws Exception {

        mockMvc.perform(put(ASSIGN_TASK_TO_USER_PATH, ID, ID)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void givenTaskNotPresentWhenAssignTaskToUserExpectErrorMessage() throws Exception {

        doThrow(new NotFoundException(TASK_NOT_FOUND_EXCEPTION_MESSAGE))
                .when(taskService).assignTaskToUser(ID, ID);

        mockMvc.perform(put(ASSIGN_TASK_TO_USER_PATH, ID, ID)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(STATUS, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR, is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath(MESSAGE, is(TASK_NOT_FOUND_EXCEPTION_MESSAGE)))
                .andExpect(jsonPath(PATH, is(ERROR_ASSIGN_TASK_TO_USER_PATH)));
    }

    @Test
    public void givenUserNotPresentWhenAssignTaskToUserExpectErrorMessage() throws Exception {

        doThrow(new NotFoundException(USER_NOT_FOUND_EXCEPTION_MESSAGE))
                .when(taskService).assignTaskToUser(ID, ID);

        mockMvc.perform(put(ASSIGN_TASK_TO_USER_PATH, ID, ID)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(STATUS, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR, is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath(MESSAGE, is(USER_NOT_FOUND_EXCEPTION_MESSAGE)))
                .andExpect(jsonPath(PATH, is(ERROR_ASSIGN_TASK_TO_USER_PATH)));
    }
}
