package com.example.etaskify.web.rest;

import com.example.etaskify.repository.UserRepository;
import com.example.etaskify.services.UserService;
import com.example.etaskify.services.dto.UserDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static com.example.etaskify.config.GlobalExceptionHandler.ERROR;
import static com.example.etaskify.config.GlobalExceptionHandler.ERRORS;
import static com.example.etaskify.config.GlobalExceptionHandler.MESSAGE;
import static com.example.etaskify.config.GlobalExceptionHandler.PATH;
import static com.example.etaskify.config.GlobalExceptionHandler.STATUS;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
@WithMockUser(authorities = {"ADMIN", "USER"})
class UserControllerTest {

    private static final Long ID = 1L;
    private static final String EMAIL = "info@example.com";
    private static final String PASSWORD = "password";
    private static final String NAME = "firstname";
    private static final String SURNAME = "lastname";
    private static final String USERNAME = "username";

    private static final String CREATE_USER_PATH = "/admin/organization/{ownerId}/user";
    private static final String ERROR_USER_PATH = "/admin/organization/1/user";
    private static final String ARGUMENT_VALIDATION_FAILED = "Argument validation failed";
    private static final String ERRORS_PROPERTY = ERRORS + "[0].property";
    private static final String ERRORS_MESSAGE = ERRORS + "[0].message";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    @SuppressWarnings({"PMD.UnusedPrivateField"})
    private UserService userService;

    @MockBean
    @SuppressWarnings({"PMD.UnusedPrivateField"})
    private UserRepository userRepository;

    private UserDto userDto;

    @BeforeEach
    @SuppressWarnings("checkstyle:methodlength")
    void setUp() {

        userDto = new UserDto();
        userDto.setUserName(USERNAME);
        userDto.setEmail(EMAIL);
        userDto.setName(NAME);
        userDto.setSurname(SURNAME);
        userDto.setPassword(PASSWORD);
    }

    @Test
    public void givenUserIsValidWhenCreateThenStatusCreated() throws Exception {

        mockMvc.perform(post(CREATE_USER_PATH, ID)
                .content(objectMapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());

    }

    @Test
    public void givenUsernameNotPresentWhenCreateExpectErrorMessage() throws Exception {

        userDto.setUserName(null);

        mockMvc.perform(post(CREATE_USER_PATH, ID)
                .content(objectMapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(STATUS, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR, is(HttpStatus.BAD_REQUEST.getReasonPhrase())))
                .andExpect(jsonPath(MESSAGE, is(ARGUMENT_VALIDATION_FAILED)))
                .andExpect(jsonPath(ERRORS).isArray())
                .andExpect(jsonPath(ERRORS, hasSize(1)))
                .andExpect(jsonPath(ERRORS_PROPERTY, is("userName")))
                .andExpect(jsonPath(ERRORS_MESSAGE, is("username is required")))
                .andExpect(jsonPath(PATH, is(ERROR_USER_PATH)));
    }

    @Test
    public void givenUserEmailNotPresentWhenCreateThenExpectErrorMessage() throws Exception {

        userDto.setEmail(null);

        mockMvc.perform(post(CREATE_USER_PATH, ID)
                .content(objectMapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(STATUS, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR, is(HttpStatus.BAD_REQUEST.getReasonPhrase())))
                .andExpect(jsonPath(MESSAGE, is(ARGUMENT_VALIDATION_FAILED)))
                .andExpect(jsonPath(ERRORS).isArray())
                .andExpect(jsonPath(ERRORS, hasSize(1)))
                .andExpect(jsonPath(ERRORS_PROPERTY, is("email")))
                .andExpect(jsonPath(ERRORS_MESSAGE, is("email is required")))
                .andExpect(jsonPath(PATH, is(ERROR_USER_PATH)));
    }

    @Test
    public void givenUserFirstNameNotPresentWhenCreateUserExpectErrorMessage() throws Exception {

        userDto.setName(null);

        mockMvc.perform(post(CREATE_USER_PATH, ID)
                .content(objectMapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(STATUS, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR, is(HttpStatus.BAD_REQUEST.getReasonPhrase())))
                .andExpect(jsonPath(MESSAGE, is(ARGUMENT_VALIDATION_FAILED)))
                .andExpect(jsonPath(ERRORS).isArray())
                .andExpect(jsonPath(ERRORS, hasSize(1)))
                .andExpect(jsonPath(ERRORS_PROPERTY, is("name")))
                .andExpect(jsonPath(ERRORS_MESSAGE, is("name is required")))
                .andExpect(jsonPath(PATH, is(ERROR_USER_PATH)));
    }

    @Test
    public void givenUserLastNameNotPresentWhenCreateUserExpectErrorMessage() throws Exception {

        userDto.setSurname(null);

        mockMvc.perform(post(CREATE_USER_PATH, ID)
                .content(objectMapper.writeValueAsString(userDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(STATUS, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR, is(HttpStatus.BAD_REQUEST.getReasonPhrase())))
                .andExpect(jsonPath(MESSAGE, is(ARGUMENT_VALIDATION_FAILED)))
                .andExpect(jsonPath(ERRORS).isArray())
                .andExpect(jsonPath(ERRORS, hasSize(1)))
                .andExpect(jsonPath(ERRORS_PROPERTY, is("surname")))
                .andExpect(jsonPath(ERRORS_MESSAGE, is("surname is required")))
                .andExpect(jsonPath(PATH, is(ERROR_USER_PATH)));
    }
}
