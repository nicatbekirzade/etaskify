package com.example.etaskify.web.rest;

import com.example.etaskify.exception.AlreadyExistException;
import com.example.etaskify.exception.NotFoundException;
import com.example.etaskify.repository.UserRepository;
import com.example.etaskify.services.OrganizationService;
import com.example.etaskify.services.UserService;
import com.example.etaskify.services.dto.AddressDto;
import com.example.etaskify.services.dto.AdminDto;
import com.example.etaskify.services.dto.OrganizationDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static com.example.etaskify.config.GlobalExceptionHandler.ERROR;
import static com.example.etaskify.config.GlobalExceptionHandler.MESSAGE;
import static com.example.etaskify.config.GlobalExceptionHandler.PATH;
import static com.example.etaskify.config.GlobalExceptionHandler.STATUS;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(OrganizationController.class)
@WithMockUser(authorities = {"ADMIN", "USER"})
class OrganizationControllerTest {

    private static final Long ID = 1L;
    private static final String ORGANIZATION_NAME = "organization_name";
    private static final String PHONE = "+994";
    private static final String CITY = "city";
    private static final String PLACE = "place";
    private static final String EMAIL = "info@example.com";
    private static final String PASSWORD = "password";
    private static final String USERNAME = "username";
    private static final String CREATE_ORGANIZATION_PATH = "/organization";
    private static final String CREATE_OWNER_PATH = "/organization/{organizationId}/admin";
    private static final String ERROR_OWNER_PATH = "/organization/1/admin";

    private static final String ORGANIZATION_ALREADY_EXIST_ERROR_MESSAGE =
            String.format("Organization with name: '%s' already exists", ORGANIZATION_NAME);
    private static final String ORGANIZATION_NOT_EXIST_ERROR_MESSAGE =
            String.format("Organization with: '%d' does not exist", ID);

    @MockBean
    private OrganizationService organizationService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserService userService;

    @MockBean
    @SuppressWarnings({"PMD.UnusedPrivateField"})
    private UserRepository userRepository;

    private OrganizationDto organizationDto;
    private AdminDto adminDto;

    @Test
    public void givenOrganizationWhenCreateThenStatusOk() throws Exception {

        mockMvc.perform(post(CREATE_ORGANIZATION_PATH)
                .content(objectMapper.writeValueAsString(organizationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    @Test
    public void givenOrganizationAlreadyExistWhenCreateOrganizationExpectErrorMessage() throws Exception {

        doThrow(new AlreadyExistException(ORGANIZATION_ALREADY_EXIST_ERROR_MESSAGE)).when(organizationService)
                .createOrganization(organizationDto);

        mockMvc.perform(post(CREATE_ORGANIZATION_PATH)
                .content(objectMapper.writeValueAsString(organizationDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(STATUS, is(HttpStatus.BAD_REQUEST.value())))
                .andExpect(jsonPath(ERROR, is(HttpStatus.BAD_REQUEST.getReasonPhrase())))
                .andExpect(jsonPath(MESSAGE, is(ORGANIZATION_ALREADY_EXIST_ERROR_MESSAGE)))
                .andExpect(jsonPath(PATH, is(CREATE_ORGANIZATION_PATH)));
    }

    @Test
    public void givenUserOwnerWhenCreateOwnerThenStatusCreated() throws Exception {

        mockMvc.perform(post(CREATE_OWNER_PATH, ID)
                .content(objectMapper.writeValueAsString(adminDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());
    }

    @Test
    public void givenOrganizationIsNotPresentWhenCreateOwnerThenThrowNotFoundException() throws Exception {

        doThrow(new NotFoundException(ORGANIZATION_NOT_EXIST_ERROR_MESSAGE)).when(userService)
                .createOwner(ID, adminDto);

        mockMvc.perform(post(CREATE_OWNER_PATH, ID)
                .content(objectMapper.writeValueAsString(adminDto))
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath(STATUS, is(HttpStatus.NOT_FOUND.value())))
                .andExpect(jsonPath(ERROR, is(HttpStatus.NOT_FOUND.getReasonPhrase())))
                .andExpect(jsonPath(MESSAGE, is(ORGANIZATION_NOT_EXIST_ERROR_MESSAGE)))
                .andExpect(jsonPath(PATH, is(ERROR_OWNER_PATH)));
    }

    @BeforeEach
    void setUp() {

        AddressDto addressDto = new AddressDto();
        addressDto.setCity(CITY);
        addressDto.setPlace(PLACE);

        organizationDto = new OrganizationDto();
        organizationDto.setName(ORGANIZATION_NAME);
        organizationDto.setPhone(PHONE);
        organizationDto.setAddress(addressDto);

        adminDto = new AdminDto();
        adminDto.setEmail(EMAIL);
        adminDto.setPassword(PASSWORD);
        adminDto.setUserName(USERNAME);
    }
}
