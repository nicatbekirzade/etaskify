## eTaskify
eTaskify, is a cloud based enterprise task manager platform for organizations where companies 
can manage their daily tasks online.

To run the application properly the user must have **docker** and **docker-compose** installed on the machine
After installation just open the project folder via terminal.
run `docker-compose up` and then **start** the application.
<img src="/uploads/ed9fbd1b210164906d19a882499735da/image.png"  width="600" >

After running aplication you can simply check APIs with :
http://localhost:8080/swagger-ui.html

## Features:
Anyone can sign up to create own organization profile 
Then you can organize your staff and tasks on the eTaskify platform

1. Who has ADMIN role can:

    - Create users
    - Create tasks and assign tasks to any user
    - Switch task status as completed/in-progress
    - Assign task to users
    - List all tasks
    - Delete tasks

2. Another signed in USERS can:

    - Switch task status as completed/in-progress
    - List all tasks


## Default Users: 
1. user_name: admin  password admin

2. user_name: user  password user
